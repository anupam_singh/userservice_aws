package com.stackOverFlow;

import java.util.List;

import io.vertx.core.AsyncResult;
import io.vertx.core.Handler;
import io.vertx.core.json.JsonObject;

public interface MyMongoAuth {
	
	String DEFAULT_NAME_FIELD = "name";
	
	String DEFAULT_CITY_FIELD = "city";
	
	String PROPERTY_NAME_FIELD = "namefield";
	
	String PROPERTY_CITY_FIELD = "cityfield";
	
	String DEFAULT_EMAIL_FIELD = "email";
	
	String PROPERTY_EMAIL_FIELD = "emailfield";

	void insertUser(JsonObject userprofile, List<String> roles, List<String> permissions,
			Handler<AsyncResult<String>> resultHandler);
	
	MyMongoAuth setFirstNameField(String firstname);
	
	MyMongoAuth setCityField(String city);
	
	MyMongoAuth setEmailField(String email);
	
	String getFirstNameField();
	
	String getCityField();
	
	String getEmailField();

}
