package com.stackOverFlow;

import io.vertx.core.Future;
import io.vertx.ext.web.Router;
import io.vertx.ext.web.handler.SessionHandler;
import io.vertx.ext.web.sstore.ClusteredSessionStore;

public class StagingUserService extends DefaultUserService {
	
	private static final String SERVICE_NAME = "userservice";

	@Override
	protected void createSessionHandler(Router router) {
		router.route().handler(SessionHandler.create(ClusteredSessionStore.create(vertx)));
	}

	@Override
	protected String getMongoDBHost() {
		//return "localhost";
		 return "mongodbhost";
	}

	@Override
	protected void createHttpServer(Router router, Future<Void> future) {
		String host = config().getString("http.address", "localhost");
		int port = config().getInteger("http.port", DEFAULT_PORT);
		System.out.println("StagingUserService.start() host: "+host);
		// create HTTP server and publish REST service
		createHttpServer(router, host, port).compose(serverCreated -> publishHttpEndpoint(SERVICE_NAME, host, port,"/users/"));
	}

}
