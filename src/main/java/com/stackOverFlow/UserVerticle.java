package com.stackOverFlow;

import io.vertx.core.AbstractVerticle;
import io.vertx.core.json.Json;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.auth.mongo.MongoAuth;
import io.vertx.ext.mongo.FindOptions;
import io.vertx.ext.mongo.MongoClient;
import io.vertx.ext.mongo.UpdateOptions;

public class UserVerticle extends AbstractVerticle {
	
	@Override
	public void start() throws Exception {
		vertx.eventBus().consumer("read.users", message -> {
			System.out.println("UserVerticle.start() read.users message: "+message.body().toString());
			JsonObject jsonMsg = new JsonObject(message.body().toString());
			String username = jsonMsg.getString("username");
			String mongodbHost = jsonMsg.getString(DefaultUserService.MONGODB_HOST);
			System.out.println("UserVerticle.start()  read.users mongodbHost: "+mongodbHost);
			System.out.println("UserVerticle.start() read.users username: " +username);
			JsonObject config = new JsonObject();
			config.put("db_name", "stackoverflow");
			config.put("connection_string", "mongodb://" + mongodbHost + DefaultUserService.MONGO_PORT);
			MongoClient client = MongoClient.createShared(vertx, config);
			FindOptions options = new FindOptions().setFields(new JsonObject().put("_id", true)
					.put(MyMongoAuth.DEFAULT_NAME_FIELD, true).put(MyMongoAuth.DEFAULT_CITY_FIELD, true).put(MyMongoAuth.DEFAULT_EMAIL_FIELD, true));
//			client.find("users", new JsonObject().put("username", username), res -> {
			client.findWithOptions("users", new JsonObject().put("_id", username),options, res -> {
				if (res.succeeded()) {
					if (res.result().size() == 0)
						message.fail(404, "User not found");
					else {
						JsonObject user = res.result().get(0);
						String json = Json.encodePrettily(user);
						System.out.println("User Found: " + json);
						message.reply(json);
					}
				} else {
					res.cause().printStackTrace();
					message.fail(500, "User not found");
				}
			});
		});
		
		vertx.eventBus().consumer("read.users.all", message -> {
			System.out.println("UserVerticle.start() read all users message: "+message.body().toString());
			JsonObject jsonMsg = new JsonObject(message.body().toString());
			String mongodbHost = jsonMsg.getString(DefaultUserService.MONGODB_HOST);
			System.out.println("UserVerticle.start()  read all users mongodbHost: "+mongodbHost);
			JsonObject config = new JsonObject();
			config.put("db_name", "stackoverflow");
			config.put("connection_string", "mongodb://" + mongodbHost + DefaultUserService.MONGO_PORT);
			MongoClient client = MongoClient.createShared(vertx, config);
			FindOptions options = new FindOptions().setFields(new JsonObject().put("_id", true)
					.put(MyMongoAuth.DEFAULT_NAME_FIELD, true));
//			client.find("users", new JsonObject().put("username", username), res -> {
			client.findWithOptions("users", new JsonObject(),options, res -> {
				if (res.succeeded()) {
					if (res.result().size() == 0)
						message.fail(404, "No User found");
					else {
						message.reply(new JsonArray(res.result()));
					}
				} else {
					res.cause().printStackTrace();
					message.fail(500, "No User found");
				}
			});
		});
		
		vertx.eventBus().consumer("update.users", message -> {
			System.out.println("UserVerticle.start() update.users");
			JsonObject jsonMsg = new JsonObject(message.body().toString());
			String username = jsonMsg.getString("username");
			String mongodbHost = jsonMsg.getString(DefaultUserService.MONGODB_HOST);
			UpdateOptions updateOptions = new UpdateOptions().setUpsert(false);
			JsonObject query = new JsonObject().put("_id", username);
			System.out.println("UserVerticle.start() jsonMsg: "+jsonMsg.encodePrettily());
			JsonObject update = new JsonObject().put("$set", jsonMsg);
			
			JsonObject config = new JsonObject();
			config.put("db_name", "stackoverflow");
			config.put("connection_string", "mongodb://" + mongodbHost + DefaultUserService.MONGO_PORT);
			MongoClient client = MongoClient.createShared(vertx, config);
			jsonMsg.remove("username");
			jsonMsg.remove(DefaultUserService.MONGODB_HOST);
			client.updateCollectionWithOptions("users", query, update, updateOptions, res -> {
				System.out.println("UserVerticle.start() res.succeeded(): "+res.succeeded());
				if (res.succeeded()) {
					message.reply("User Profile is UPDATED with id " + res.result());
				} else {
					System.out.println("UserVerticle.start() res.cause(): "+res.cause());
					message.fail(500, "User Profile UPDATE failed");
				}
			});
		});
		
		
		vertx.eventBus().consumer("write.users", message -> {
			JsonObject jsonMsg = new JsonObject(message.body().toString());
			String username = jsonMsg.getString(MongoAuth.DEFAULT_USERNAME_FIELD);
			String password = jsonMsg.getString(MongoAuth.DEFAULT_PASSWORD_FIELD);
			String firstname = jsonMsg.getString(MyMongoAuth.DEFAULT_NAME_FIELD);
			String city = jsonMsg.getString("city");
			String email = jsonMsg.getString("email");
			System.out.println("UserVerticle.start() write.users jsonMsg.getValue of username: "+jsonMsg.getValue("username"));
			System.out.println("UserVerticle.start() write.users username: "+username + " password: "+password);
			if(username == null || password == null) {
				message.fail(500, "Registration failed as username or password is empty");
				return;
			}
			String mongodbHost = jsonMsg.getString(DefaultUserService.MONGODB_HOST);
			System.out.println("UserVerticle.start()  write.users mongodbHost: "+mongodbHost);
			JsonObject config = new JsonObject();
			config.put("db_name", "stackoverflow");
			config.put("connection_string", "mongodb://" + mongodbHost + DefaultUserService.MONGO_PORT);
			MongoClient client = MongoClient.createShared(vertx, config);
			JsonObject authProperties = new JsonObject();
			authProperties.put(MongoAuth.PROPERTY_COLLECTION_NAME, "users");
			authProperties.put(MongoAuth.PROPERTY_USERNAME_FIELD, "_id");
			authProperties.put(MongoAuth.DEFAULT_USERNAME_FIELD, username);
			authProperties.put(MongoAuth.DEFAULT_PASSWORD_FIELD, password);
			authProperties.put(MyMongoAuth.DEFAULT_NAME_FIELD, firstname);
			authProperties.put(MyMongoAuth.DEFAULT_CITY_FIELD, city);
			authProperties.put(MyMongoAuth.DEFAULT_EMAIL_FIELD, email);
//		    MongoAuth authProvider = MongoAuth.create(client, authProperties);
			MyMongoAuth authProvider = new MyAuthProvider(client, authProperties);	
		    authProvider.insertUser(authProperties, null, null, res -> {
		    	if(res.succeeded()) {
		    		message.reply("USER is registered");
		    	} else {
		    		message.fail(500, "Registration failed");
		    	}
		    });
		    
		});
		
	}

}
