package com.stackOverFlow;

import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.temporal.ChronoUnit;
import java.util.concurrent.atomic.AtomicInteger;

import com.stackOverFlow.common.RestAPIVerticle;

import io.vertx.core.DeploymentOptions;
import io.vertx.core.Future;
import io.vertx.core.Handler;
import io.vertx.core.eventbus.ReplyException;
import io.vertx.core.http.HttpMethod;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.auth.jwt.JWTAuth;
import io.vertx.ext.auth.mongo.MongoAuth;
import io.vertx.ext.mongo.MongoClient;
import io.vertx.ext.web.Router;
import io.vertx.ext.web.RoutingContext;
import io.vertx.ext.web.handler.BodyHandler;
import io.vertx.ext.web.handler.CookieHandler;
import io.vertx.ext.web.handler.CorsHandler;
import io.vertx.ext.web.handler.FormLoginHandler;
import io.vertx.ext.web.handler.JWTAuthHandler;
import io.vertx.rabbitmq.RabbitMQClient;
import io.vertx.rabbitmq.impl.RabbitMQClientImpl;
import io.vertx.redis.RedisClient;
import io.vertx.redis.RedisOptions;

public abstract class DefaultUserService extends RestAPIVerticle {
	
	public static final String MONGO_PORT = ":27017";
	
	public static final String MONGODB_HOST = "MONGODB_HOST";
	
	protected static final int DEFAULT_PORT = 8050;

	@Override
	public void start(Future<Void> future) throws Exception {
		super.start();
		Router router = Router.router(vertx);
		// body handler
		router.route().handler(CookieHandler.create());
		router.route().handler(BodyHandler.create());
		createSessionHandler(router);
		vertx.deployVerticle("com.stackOverFlow.UserVerticle", new DeploymentOptions().setWorker(true).setInstances(2));
		
		router.route().handler(CorsHandler.create("*")
			      .allowedMethod(HttpMethod.GET)
			      .allowedMethod(HttpMethod.POST)
			      .allowedMethod(HttpMethod.PUT)
			      .allowedMethod(HttpMethod.DELETE)
			      .allowedMethod(HttpMethod.OPTIONS)
			      .allowCredentials(true)
			      .allowedHeader("X-PINGARUNER")
			      .allowedHeader("Access-Control-Allow-Method")
			      .allowedHeader("Access-Control-Allow-Origin")
			      .allowedHeader("Access-Control-Allow-Credentials")
			      .allowedHeader("Authorization")
			      .allowedHeader("Content-Type"));

		JsonObject config = new JsonObject();
		config.put("db_name", "stackoverflow");
		// config.put("connection_string", "mongodb://" + MONGODB + ":27017");
		config.put("connection_string", "mongodb://" + getMongoDBHost() + MONGO_PORT);
		MongoClient client = MongoClient.createShared(vertx, config);
		JsonObject authProperties = new JsonObject();
		authProperties.put(MongoAuth.PROPERTY_COLLECTION_NAME, "users");
		authProperties.put(MongoAuth.PROPERTY_USERNAME_FIELD, "_id");
		// MongoAuth authProvider = MongoAuth.create(client, authProperties);
		MongoAuth authProvider = new MyAuthProvider(client, authProperties);
		
		RedisOptions redis_config = new RedisOptions().setHost("35.188.227.50");
//		RedisOptions config = new RedisOptions().setHost("10.197.54.23:8030");
		RedisClient redis = RedisClient.create(vertx, redis_config);

		// Create a JWT Auth Provider
		JWTAuth jwt = JWTAuth.create(vertx, new JsonObject().put("keyStore",
				new JsonObject().put("type", "jceks").put("path", "keystore.jceks").put("password", "secret")));

		Handler<RoutingContext> handler = JWTAuthHandler.create(jwt);
		router.route(HttpMethod.GET, "/users/").handler(handler);
		router.route(HttpMethod.GET, "/users/chat/").handler(handler);
		router.route(HttpMethod.GET, "/users/logout/").handler(handler);
		router.route(HttpMethod.GET, "/users/profile/").handler(handler);
		router.route(HttpMethod.PUT, "/users/").handler(handler);
//		System.out.println("DefaultUserService.start() authProvider: " + authProvider);
		router.route("/users/login")
				.handler(new CustomFormLoginHandler(authProvider, FormLoginHandler.DEFAULT_USERNAME_PARAM,
						FormLoginHandler.DEFAULT_PASSWORD_PARAM, FormLoginHandler.DEFAULT_RETURN_URL_PARAM, null, jwt));

		router.get("/users/").handler(rctx -> {
			System.out.println("DefaultUserService.start() GET USERS rctx.user(): " + rctx.user());
			if (rctx.user() == null) {
				rctx.response().setStatusCode(401).putHeader("Content-Type", "application/json").end("Not Authorized");
				return;
			}
			System.out.println("DefaultUserService.start() rctx.user().principal(): " + rctx.user().principal());
			JsonObject principalObj = rctx.user().principal();
			principalObj.put("MONGODB_HOST", getMongoDBHost());
			vertx.eventBus().send("read.users", principalObj, res -> {
				if (res.succeeded()) {
					rctx.response().setStatusCode(200).putHeader("Content-Type", "application/json")
							.end(res.result().body().toString());
				} else if (res.failed()) {
					rctx.response().setStatusCode(((ReplyException) res.cause()).failureCode())
							.putHeader("Content-Type", "application/json").end(res.cause().getLocalizedMessage());
				}
			});
		});
		
		router.get("/users/chat/").handler(rctx -> {
			System.out.println("DefaultUserService.start() GET ALL USERSS rctx.user(): " + rctx.user());
			if (rctx.user() == null) {
				rctx.response().setStatusCode(401).putHeader("Content-Type", "application/json").end("Not Authorized");
				return;
			}
			System.out.println("DefaultUserService.start() GET ALL USERS rctx.user().principal(): " + rctx.user().principal());
			JsonObject principalObj = rctx.user().principal();
			String loggedinUsername = principalObj.getString("username");
			redis.smembers("chat_users_set", response -> {
				System.out.println("ChatService.start() RedisClient SMEMBERS CHAT USERS succeeded"+response.succeeded());
				if(response.succeeded()) {
					JsonArray chatUsersArray = new JsonArray();
					JsonArray usersJsonArray = response.result();
					System.out.println("DefaultUserService.start() usersJsonArray.encodePrettily(): "+usersJsonArray.encodePrettily());
					System.out.println("DefaultUserService.start() usersJsonArray.size(): "+usersJsonArray.size());
					final AtomicInteger count = new AtomicInteger();
					for (int i = 0; i < usersJsonArray.size(); i++) {
						String user_id = usersJsonArray.getString(i);
						redis.get(user_id, response_2 -> {
							JsonObject userInfoObj = new JsonObject(response_2.result());
							System.out.println("DefaultUserService.start() userInfoObj.encodePrettily(): "+userInfoObj.encodePrettily());
							if(!loggedinUsername.equals(user_id)) {
								userInfoObj.put("_id", user_id);
								if(userInfoObj.containsKey("lastactive")) {
									modifyDate(userInfoObj, "lastactive");
								}
								chatUsersArray.add(userInfoObj);
							}
							count.set(count.get() + 1);
							if(count.get() == usersJsonArray.size()) {
								System.out.println("DefaultUserService.start() chatUsersArray.size(): "+chatUsersArray.size());
								if(chatUsersArray.size() > 0) {
									rctx.response().setStatusCode(200).putHeader("Content-Type", "application/json").end(chatUsersArray.encodePrettily());
								} else {
									rctx.response().setStatusCode(500).putHeader("Content-Type", "application/json").end("No Chat Users found");
								}
							}
						});
					}
					
				} else {
					rctx.response().setStatusCode(500).putHeader("Content-Type", "application/json").end("No Chat Users found");
				}
			});
//			redis.get("online_users_info", response-> {
//				System.out.println("ChatService.start() RedisClient GET ONLINE USERS succeeded"+response.succeeded());
//				if(response.succeeded()) {
////					JsonArray jsonUsers = new JsonArray(response.result());
//					rctx.response().setStatusCode(200).putHeader("Content-Type", "application/json")
//					.end(response.result());
//				} else {
//					rctx.response().setStatusCode(500)
//					.putHeader("Content-Type", "application/json").end("No Chat Users found");
//				}
//			});
//			System.out.println("DefaultUserService.start() GET ALL USERS rctx.user().principal(): " + rctx.user().principal());
//			JsonObject mongodbObj = rctx.user().principal();
//			mongodbObj.put("MONGODB_HOST", getMongoDBHost());
//			vertx.eventBus().send("read.users.all", mongodbObj, res -> {
//				if (res.succeeded()) {
//					rctx.response().setStatusCode(200).putHeader("Content-Type", "application/json")
//							.end(res.result().body().toString());
//				} else if (res.failed()) {
//					rctx.response().setStatusCode(((ReplyException) res.cause()).failureCode())
//							.putHeader("Content-Type", "application/json").end(res.cause().getLocalizedMessage());
//				}
//			});
		});
		
		router.get("/users/profile/").handler(rctx -> {
			System.out.println("DefaultUserService.start() GET USERS DETAIL rctx.user(): " + rctx.user());
			if (rctx.user() == null) {
				rctx.response().setStatusCode(401).putHeader("Content-Type", "application/json").end("Not Authorized");
				return;
			}
			System.out.println("DefaultUserService.start()USERS DETAIL rctx.user().principal(): " + rctx.user().principal());
			JsonObject principalObj = rctx.user().principal();
			principalObj.put("MONGODB_HOST", getMongoDBHost());
			vertx.eventBus().send("read.users", principalObj, res -> {
				System.out.println("DefaultUserService.start() USERS DETAIL res.succeeded(): "+res.succeeded());
				if (res.succeeded()) {
					rctx.response().setStatusCode(200).putHeader("Content-Type", "application/json")
							.end(res.result().body().toString());
				} else if (res.failed()) {
					rctx.response().setStatusCode(((ReplyException) res.cause()).failureCode())
							.putHeader("Content-Type", "application/json").end(res.cause().getLocalizedMessage());
				}
			});
		});
		
		router.get("/users/profile/:userId").handler(rctx -> {
			System.out.println("APIGateway_2.start() USER profile:::::::::::: "+rctx.request().getParam("userId"));
			JsonObject jsonObj = new JsonObject().put("username", rctx.request().getParam("userId"))
					.put("MONGODB_HOST", getMongoDBHost());
			vertx.eventBus().send("read.users", jsonObj, res -> {
				System.out.println("DefaultUserService.start() USERS PROFILE res.succeeded(): "+res.succeeded());
				if (res.succeeded()) {
					rctx.response().setStatusCode(200).putHeader("Content-Type", "application/json")
							.end(res.result().body().toString());
				} else if (res.failed()) {
					rctx.response().setStatusCode(((ReplyException) res.cause()).failureCode())
							.putHeader("Content-Type", "application/json").end(res.cause().getLocalizedMessage());
				}
			});
		});

		router.post("/users/").handler(rctx -> {
			System.out.println("DefaultUserService.start() POST USERS rctx.getBodyAsJson().encodePrettily(): "
					+ rctx.getBodyAsJson().encodePrettily());
			JsonObject userJsonObj = rctx.getBodyAsJson();
			userJsonObj.put("MONGODB_HOST", getMongoDBHost());
			vertx.eventBus().send("write.users", userJsonObj.encodePrettily(), res -> {
				if (res.succeeded()) {
					rctx.response().setStatusCode(200).putHeader("Content-Type", "application/json")
							.end(res.result().body().toString());
				} else if (res.failed()) {
					rctx.response().setStatusCode(((ReplyException) res.cause()).failureCode())
							.putHeader("Content-Type", "application/json").end(res.cause().getLocalizedMessage());
				}
			});
		});
		
		router.put("/users/").handler(rctx -> {
			System.out.println("DefaultUserService.start() PUT USERS rctx.getBodyAsJson().encodePrettily(): "
					+ rctx.getBodyAsJson().encodePrettily());
			JsonObject userJsonObj = rctx.getBodyAsJson();
			userJsonObj.put("MONGODB_HOST", getMongoDBHost());
			vertx.eventBus().send("update.users", userJsonObj.encodePrettily(), res -> {
				if (res.succeeded()) {
					userJsonObj.remove("MONGODB_HOST");
//					RabbitMQClient rabbit = new RabbitMQClientImpl(vertx, new JsonObject().put("uri", "amqp://35.190.147.51:5672"));
					RabbitMQClient rabbit = new RabbitMQClientImpl(vertx, new JsonObject().put("uri", "amqp://10.197.54.23:8090"));
//					RabbitMQClient rabbit = new RabbitMQClientImpl(vertx, new JsonObject().put("uri", "amqp://104.196.128.2:8090"));
					rabbit.start(response -> {
						JsonObject message = new JsonObject();
						message.put("properties", new JsonObject().put("contentType", "application/json"));
						message.put("body", userJsonObj);
						rabbit.basicPublish("", "cmad.queue", message, result -> {
							if (result.succeeded()) {
								System.out.println("Message Published!");
							} else {
								System.out.println("Failed");
								result.cause().printStackTrace();
							}
						});
					});
					rctx.response().setStatusCode(200).putHeader("Content-Type", "application/json")
							.end(res.result().body().toString());
				} else if (res.failed()) {
					rctx.response().setStatusCode(((ReplyException) res.cause()).failureCode())
							.putHeader("Content-Type", "application/json").end(res.cause().getLocalizedMessage());
				}
			});
		});

		router.get("/users/logout/").handler(rctx -> {
			System.out
					.println("DefaultUserService.start() Authorization header: " + rctx.request().getHeader("Authorization"));
			rctx.clearUser();
			rctx.response().setStatusCode(200).putHeader("Content-Type", "text/html").end();
		});
		
		router.route(HttpMethod.GET, "/questions/me/").handler(handler);

		router.route(HttpMethod.POST, "/questions/").handler(handler);
//		router.route(HttpMethod.PATCH, "/questions/").handler(handler);
		router.route(HttpMethod.PUT, "/questions/").handler(handler);

		router.route(HttpMethod.POST, "/questions/").handler(rctx -> {
			System.out.println("DefaultUserService.start() POST USERS rctx.user(): " + rctx.user());
			if (rctx.user() == null) {
				rctx.response().setStatusCode(401).putHeader("Content-Type", "application/json").end("Not Authorized");
				return;
			}
			JsonObject principalObj = rctx.user().principal();
			principalObj.put("MONGODB_HOST", getMongoDBHost());
			vertx.eventBus().send("read.users", principalObj, res -> {
				if (res.succeeded()) {
					JsonObject resJson = new JsonObject(res.result().body().toString());
					JsonObject usernameObj = new JsonObject();
					usernameObj.put("username", resJson.getString("_id"));
					usernameObj.put("name", resJson.getString("name"));
					usernameObj.put("city", resJson.getString("city"));
					rctx.response().setStatusCode(404).putHeader("Content-Type", "application/json")
							.end(usernameObj.encodePrettily());
				} else if (res.failed()) {
					rctx.response().setStatusCode(((ReplyException) res.cause()).failureCode())
							.putHeader("Content-Type", "application/json").end(res.cause().getLocalizedMessage());
				}
			});
		});
		
		router.route(HttpMethod.GET, "/questions/me/").handler(rctx -> {
			System.out.println("DefaultUserService.start() GET USERS for MY QUESTIONS rctx.user(): " + rctx.user());
			if (rctx.user() == null) {
				rctx.response().setStatusCode(401).putHeader("Content-Type", "application/json").end("Not Authorized");
				return;
			}
			
			JsonObject principalObj = rctx.user().principal();
			JsonObject usernameObj = new JsonObject();
			usernameObj.put("username", principalObj.getString("username"));
			rctx.response().setStatusCode(404).putHeader("Content-Type", "application/json")
			.end(usernameObj.encodePrettily());
		});

//		router.route(HttpMethod.PATCH, "/questions/").handler(rctx -> {
		router.route(HttpMethod.PUT, "/questions/").handler(rctx -> {
			System.out.println("DefaultUserService.start() PATCH USERS rctx.user(): " + rctx.user());
			if (rctx.user() == null) {
				rctx.response().setStatusCode(401).putHeader("Content-Type", "application/json").end("Not Authorized");
				return;
			}
			JsonObject principalObj = rctx.user().principal();
			principalObj.put("MONGODB_HOST", getMongoDBHost());
			vertx.eventBus().send("read.users", principalObj, res -> {
				if (res.succeeded()) {
					JsonObject resJson = new JsonObject(res.result().body().toString());
					JsonObject usernameObj = new JsonObject();
					usernameObj.put("username", resJson.getString("_id"));
					usernameObj.put("name", resJson.getString("name"));
					usernameObj.put("city", resJson.getString("city"));
					rctx.response().setStatusCode(404).putHeader("Content-Type", "application/json")
							.end(usernameObj.encodePrettily());
				} else if (res.failed()) {
					rctx.response().setStatusCode(((ReplyException) res.cause()).failureCode())
							.putHeader("Content-Type", "application/json").end(res.cause().getLocalizedMessage());
				}
			});
		});
		
		createHttpServer(router, future);
	}

	protected abstract void createSessionHandler(final Router router);
	
	protected abstract String getMongoDBHost();
	
	protected abstract void createHttpServer(final Router router,Future<Void> future);
	
	private JsonObject modifyDate(JsonObject jsonObj, String lastActiveKey) {
		System.out.println("QAReadVerticle.modifyDate() key: "+lastActiveKey);
		System.out.println("QAReadVerticle.modifyDate() jsonObj.getString(key): "+jsonObj.getString(lastActiveKey));
		Instant asked = Instant.parse(jsonObj.getString(lastActiveKey));
//		Instant asked = jsonObj.getInstant(key);
		System.out.println("QAReadVerticle.modifyDate() postedDate: "+asked);
		LocalDateTime date_time = LocalDateTime.ofInstant(asked, ZoneId.systemDefault());
		System.out.println("QAReadVerticle.start() getDayOfMonth: "+date_time.getDayOfMonth() + " getDayOfYear: " + date_time.getDayOfYear() + 
				" getHour: " + date_time.getHour() + " getMinute: " + date_time.getMinute()+ " getMonthValue: " + date_time.getMonthValue()+
				" getYear: " + date_time.getYear()+ " getDayOfWeek: " + date_time.getDayOfWeek() + " getMonth: " + date_time.getMonth());
		long seconds = ChronoUnit.SECONDS.between(asked, Instant.now());
		System.out.println("QAReadVerticle.start() seconds: "+seconds);
		String key = "active";
		if(seconds > 0 && seconds < 60) {
			jsonObj.put(key, key + " " + seconds + " secs ago");
		} else if (seconds >= 60 ) {
			long minutes = ChronoUnit.MINUTES.between(asked, Instant.now());
			if(minutes < 60) {
				jsonObj.put(key, key + " " +  minutes + " minutes ago");
			} else if(minutes > 60) {
				long hours = ChronoUnit.HOURS.between(asked, Instant.now());
				if(hours < 24) {
					jsonObj.put(key, key + " " + hours + " hours ago");
				} else {
//					date and time in year-date-hour-minutes
					String value = getYear_Month_time(key,date_time);
					jsonObj.put(key, value);
				}
			}
		} else if (seconds < 0) {
//			date and time in year-date-hour-minutes
			String value = getYear_Month_time(key,date_time);
			jsonObj.put(key, value);
		}else if (seconds == 0) {
			jsonObj.put(key, key + " " + " just a while ago");
		}
		return jsonObj;
	}
	
	private String getYear_Month_time(String key,LocalDateTime date_time) {
		return key + " " + date_time.getMonth() + " " + date_time.getDayOfMonth() + " " + date_time.getYear() + " " + " at " + date_time.getHour() + ":" + date_time.getMinute();
	}
}
